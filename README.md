# A parser for Quake3 game log file

The main idea of this parser is get a Quake3 log file and translate into a pretty json file with stats round by round .

## Getting Started

For now, you can clone this repo and run.

```
npm install
```
To install all dependencies.
Recommend to use Node >= 8 and npm >= 3.

### Folders and Files
```sh
├── src
│   ├── parser.js
│   ├── util
│   │   ├── death-mods.js
│   │   └── regex-patterns.js
│   ├── tests
│   │   └── main.spec.js
│   └── mock
│       └── games.log
├── .editorconfig
├── .eslintrc
├── .gitignore
├── README.md
└── package.json
```

### Parsing your file

To use is simple

```
const parser = require('./q3-parser-labs/src/parser')
parser.parser('filename.log')

```

Will generate a file JSON with name db.JSON and a file that contains start round, end round and kill lines with friendly writing.

JSON file looks like

```
[
   {
      "id":1,
      "startAt":"0:00",
      "endAt":"1:47",
      "serverName":"Code Miner Server",
      "totalKills":4,
      "players":[
         "Isgalamido",
         "Mocinha",
         "Zeh",
         "Dono da Bola"
      ],
      "kills":[
         {
            "player":"Isgalamido",
            "kill":1
         },
         {
            "player":"Zeh",
            "kill":-2
         },
         {
            "player":"Dono da Bola",
            "kill":-1
         }
      ],
      "deathsBy":[
         {
            "mod":"MOD_ROCKET",
            "total":1
         },
         {
            "mod":"MOD_TRIGGER_HURT",
            "total":2
         },
         {
            "mod":"MOD_FALLING",
            "total":1
         }
      ]
   }
]
```
The new log file look like this
```
0:00 - Jogo Iniciado - Server : Code Miner Server
Isgalamido matou Mocinha usando a arma Lança Granada
Zeh morreu por causa de ferimentos
Zeh morreu por causa de ferimentos
Dono da Bola - morreu devido a queda
1:47 - Fim de Jogo

```
## Running the tests

- `npm test`: run your tests in a single-run mode.
- `npm run test:tdd`: run and keep watching your test files.
- `npm run test:coverage`: prints and create html files by istanbul coverage.

## Linting your code
In order to keep the code clean and consistent use [eslint](http://eslint.org/) with [Standard preset](https://standardjs.com/)

- `npm run lint`: lint all files searching for errors.
- `npm run lint:fix`: fix automaticaly some lint errors.

#### Style Guide

- [EditorConfig](http://editorconfig.org/) - *standardize some general settings among multiple editors*
- [ESLint](http://eslint.org/) - *for reporting the patterns of code*
  - [Javascript Standard](https://standardjs.com)

#### Tests
- [Mocha](https://github.com/mochajs/mocha) - *test framework*
- [Chai](https://github.com/chaijs/chai) - *assertions*

const readline = require('readline')
const fs = require('fs')
const regex = require('./util/regex-patterns').regexPatterns
const modKill = require('./util/death-mods').deathMod

let rounds = []
let round = {}
let newLog = []
let roundCount = 1

const parser = (fileName) => {
  const file = fs.createReadStream(fileName)

  file.on('error', function (err) {
    if (err) {
      throw new Error('File not found')
    }
  })

  const rl = readline.createInterface({
    input: file,
    crlfDelay: Infinity
  })

  // Read log file line by line
  rl.on('line', (line) => {
    if (line.indexOf('Kill') > -1) {
      return killRule(line)
    } else if (line.indexOf('InitGame') > -1) {
      return initGameRule(line)
    } else if (line.indexOf('ShutdownGame') > -1) {
      return endGameRule(line)
    }
  })

  rl.on('close', () => {
    let roundJson = JSON.stringify(rounds, null, 4)
    saveFile('db.json', roundJson)
    saveNewLog(newLog)
  })
}
// Rule to get initGame
const initGameRule = (line) => {
  round = {}
  let initLine = regex.regexInitGameLine.exec(line)
  if (initLine !== null && initLine.length === 3) {
    // start new round
    round.id = roundCount
    round.startAt = initLine[1]
    round.endAt = ''
    round.serverName = initLine[2]
    round.totalKills = 0
    round.players = []
    round.kills = []
    round.deathsBy = []

    // write line in new log file
    newLog.push(`${round.startAt} - Jogo Iniciado - Server : ${round.serverName}`)
  } else {
    return 'Line is not in expected structure'
  }
}

// Rule for end game
const endGameRule = (line) => {
  let endGameLine = regex.regexEndGame.exec(line)
  if (endGameLine !== null && endGameLine.length === 3) {
    round.endAt = endGameLine[1]
    roundCount++
    rounds.push(round)
    // write line in new log file
    newLog.push(`${round.endAt} - Fim de Jogo`)
  } else {
    return 'Line is not in expected structure'
  }
}

// Rule to get the kill line
const killRule = (line) => {
  let lineGroup = regex.regexKillLine.exec(line)
  if (lineGroup !== null && lineGroup.length === 6) {
    addKill(lineGroup[3], lineGroup[4], lineGroup[5])
    addDeathBy(lineGroup[5])
    addPlayer(lineGroup[3], lineGroup[4])
  } else {
    return 'Line is not in expected structure'
  }
}

// Add kill to array
const addKill = (player1, player2, killMode) => {
  round.totalKills += 1

  let killer = {}
  killer.player = player1
  killer.kill = 1

  if (player1.indexOf('<world>') > -1) {
    killer.player = player2
    killer.kill = -1
    newLog.push(`${killer.player} morreu ${modKill[killMode]} `)
  } else {
    newLog.push(`${player1} matou ${player2} ${modKill[killMode]}`)
  }

  let playerIndex = round.kills.findIndex(x => x.player === killer.player)

  if (playerIndex < 0) {
    round.kills.push(killer)
  } else {
    round.kills[playerIndex].kill += killer.kill
  }
}

// add player to the array of players
const addPlayer = (player1, player2) => {
  let players = [player1, player2]
  players.forEach((player) => {
    if (player.indexOf('<world>') < 0) {
      let playerIndex = round.players.findIndex(x => x === player)
      if (playerIndex < 0) {
        round.players.push(player)
      }
    }
  })
}

// add kill mod to array
const addDeathBy = (deathMod) => {
  let deathBy = {}
  deathBy.mod = deathMod
  deathBy.total = 1

  let modIndex = round.deathsBy.findIndex(x => x.mod === deathMod)

  if (modIndex < 0) {
    round.deathsBy.push(deathBy)
  } else {
    round.deathsBy[modIndex].total += deathBy.total
  }
}

// save json file
const saveFile = (fileName, data) => {
  if (fs.existsSync(fileName)) {
    fs.unlink(fileName, (err) => {
      if (err) throw err
    })
  }

  fs.open(fileName, 'wx', (err, data) => {
    if (err) throw err
  })

  fs.writeFile(fileName, data, (err) => {
    if (err) throw err
  })
}

// Generate a log for init kill and end game
const saveNewLog = (newLog) => {
  let writeStream = fs.createWriteStream('game-log.txt')

  newLog.forEach(value => writeStream.write(`${value}\n`))

  writeStream.on('finish', () => {
    return 'successful save'
  })

  writeStream.on('error', (err) => { throw err })

  writeStream.end()
}

module.exports = {
  parser,
  rounds,
  round,
  killRule,
  initGameRule,
  endGameRule,
  addKill,
  addPlayer,
  addDeathBy,
  saveFile,
  saveNewLog }

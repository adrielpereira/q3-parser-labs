let parser = require('../parser')
const expect = require('chai').expect
const fs = require('fs')
const path = require('path')
const filePath = path.join(__dirname, '..', 'mock', 'games.log')

describe('Parser', () => {
  afterEach(() => {
    if (fs.existsSync('db.json')) {
      fs.unlink('db.json', (err) => {
        if (err) throw err
      })
    }
    if (fs.existsSync('game-log.txt')) {
      fs.unlink('game-log.txt', (err) => {
        if (err) throw err
      })
    }
  })
  describe('Smoke test', () => {
    it('should exist a `parser` method', () => {
      expect(parser.parser).exist
    })
    it('should exist a `initGameRule` method', () => {
      expect(parser.initGameRule).exist
    })
    it('should exist a `killRule` method', () => {
      expect(parser.killRule).exist
    })
    it('should exist a `endGameRule` method', () => {
      expect(parser.endGameRule).exist
    })
    it('should exist a `addKill` method', () => {
      expect(parser.addKill).exist
    })
    it('should exist a `addPlayer` method', () => {
      expect(parser.addPlayer).exist
    })
    it('should exist a `addDeathBy` method', () => {
      expect(parser.addDeathBy).exist
    })
    it('should exist a `saveFile` method', () => {
      expect(parser.saveFile).exist
    })
    it('should exist a `saveNewLog` method', () => {
      expect(parser.saveNewLog).exist
    })
  })
  describe('parser Method', () => {
    it('should not throw `File not found`', () => {
      expect(() => parser.parser(filePath)).to.not.throw()
    })
  })
  describe('initGameRule Method', () => {
    let rightLine = '0:00 InitGame: \\sv_floodProtect\\1\\sv_maxPing\\0\\sv_minPing\\0\\sv_maxRate\\10000\\sv_minRate\\0\\sv_hostname\\Code Miner Server\\g_gametype\\0\\sv_privateClients\\2\\sv_maxclients\\16\\sv_allowDownload\\0\\dmflags\\0\\fraglimit\\20\\timelimit\\15\\g_maxGameClients\\0\\capturelimit\\8\\version\\ioq3 1.36 linux-x86_64 Apr 12 2009\\protocol\\68\\mapname\\q3dm17\\gamename\\baseq3\\g_needpass\\0'
    let wrongLine = '0:00 Comecou o jogo'

    it('should not return `Line is not in expected structure`', () => {
      expect(parser.initGameRule(rightLine)).to.not.be.equal('Line is not in expected structure')
    })
    it('should return `Line is not in expected structure`', () => {
      expect(parser.initGameRule(wrongLine)).to.be.equal('Line is not in expected structure')
    })
    it('should propertie `startAt` not be null', () => {
      parser.initGameRule(rightLine)
      expect(parser.round.startAt).not.to.be.equal(null)
    })
    it('should propertie `serverName` not be null', () => {
      parser.initGameRule(rightLine)
      expect(parser.round.serverName).not.to.be.equal(null)
    })
  })
  describe('endGameRule Method', () => {
    let rightLine = '1:47 ShutdownGame:'
    let wrongLine = 'Fim de jogo'
    it('should not return `Line is not in expected structure`', () => {
      expect(parser.endGameRule(rightLine)).to.not.be.equal('Line is not in expected structure')
    })
    it('should return `Line is not in expected structure`', () => {
      expect(parser.endGameRule(wrongLine)).to.be.equal('Line is not in expected structure')
    })
    it('should propertie `endAt` not be null', () => {
      parser.endGameRule(rightLine)
      expect(parser.round.endAt).not.to.be.equal(null)
    })
  })
  describe('killRule Method', () => {
    let rightLine = '2:11 Kill: 2 4 6: Dono da Bola killed Zeh by MOD_ROCKET'
    let wrongLine = 'Pedro matou joao fim'
    it('should not return `Line is not in expected structure`', () => {
      expect(parser.killRule(rightLine)).to.not.be.equal('Line is not in expected structure')
    })
    it('should return `Line is not in expected structure`', () => {
      expect(parser.killRule(wrongLine)).to.be.equal('Line is not in expected structure')
    })
  })
})

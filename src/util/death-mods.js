// Dictionary of mods of death from Q3 log file
const deathMod = {
  MOD_TRIGGER_HURT: 'por causa de ferimentos',
  MOD_ROCKET_SPLASH: 'pelo estilhaços do tiro da Lança granada',
  MOD_FALLING: 'devido a queda',
  MOD_ROCKET: 'usando a  arma Lança Granada',
  MOD_RAILGUN: 'usando a  arma Laser',
  MOD_MACHINEGUN: 'usando a  arma Machine Gun',
  MOD_SHOTGUN: 'usando a  arma Espingarda',
  MOD_TELEFRAG: 'pelo Telefrag',
  MOD_BFG_SPLASH: 'pelos estilhaços do tiro da arma BFG',
  MOD_BFG: 'usando a arma BFG',
  MOD_CRUSH: 'esmagado'
}

module.exports = { deathMod }

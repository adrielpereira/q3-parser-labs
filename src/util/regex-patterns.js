const regexPatterns = {
  regexKillLine: /(\d?:\d{2}) (\w+): .*: (<?[\w\s]+>?) killed ([\w\s]+) by (\w+)/i,
  regexInitGameLine: /(\d{1,2}:\d{2}).+(?<=sv_hostname\\)([\w\s]+[^\\])/i,
  regexEndGame: /(\d{1,2}:\d{2}) (\w+)/i
}

module.exports = { regexPatterns }
